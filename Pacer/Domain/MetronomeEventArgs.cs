﻿using System;

namespace FigrollApps.Pacer.Domain
{
    public class MetronomeEventArgs : EventArgs
    {
        public MetronomeEventArgs(int ticksDone, int ticksRemaining)
        {
            TicksDone = ticksDone;
            TicksRemaining = ticksRemaining;
        }

        public int TicksDone { get; }
        public int TicksRemaining { get; }
    }
}