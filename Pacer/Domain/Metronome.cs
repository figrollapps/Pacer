﻿using System;
using System.Media;
using System.Threading;

namespace FigrollApps.Pacer.Domain
{
    public sealed class Metronome
    {
        private readonly Settings settings;
        
        private const int DefaultSpeed = 120;
        private const int ContinuousPlayMode = 0;
        private const int StoppedMode = -1;

        private readonly ManualResetEvent playStopped = new ManualResetEvent(false);
        private readonly SoundPlayer startSoundPlayer;
        private readonly SoundPlayer tickSoundPlayer;
        private readonly SoundPlayer endSoundPlayer;
        private readonly SoundPlayer strokeSoundPlayer;
        private readonly SoundPlayer stopSoundPlayer;
        private readonly Timer beatTimer;

        private int bpm;
        private int doneTicks;
        private int durationTicks;
        private int remainingTicks;

        public event EventHandler<MetronomeEventArgs> Ticked;
        private void OnMetronomeTicked(MetronomeEventArgs e)
        {
            Ticked?.Invoke(this, e);
        }

        public event EventHandler<EventArgs> Stopped;
        private void OnStopped()
        {
            Stopped?.Invoke(this, EventArgs.Empty);
        }

        public Metronome(Settings settings)
        {
            this.settings = settings;

            bpm = DefaultSpeed;
            durationTicks = StoppedMode;
            doneTicks = 0;
            remainingTicks = 0;

            startSoundPlayer = new SoundPlayer(settings.StartSound);
            tickSoundPlayer = new SoundPlayer(settings.TickSound);
            endSoundPlayer = new SoundPlayer(settings.EndSound);
            strokeSoundPlayer = new SoundPlayer("Stroke.wav");
            stopSoundPlayer = new SoundPlayer("Stop.wav");

            beatTimer = new Timer(OnTick, null, Timeout.Infinite, Timeout.Infinite);
        }

        public void Play(int speed, int duration = ContinuousPlayMode)
        {
            bpm = speed;
            doneTicks = 0;
            durationTicks = duration;
            remainingTicks = duration;

            playStopped.Reset();

            if (settings.PlayVocals)
                strokeSoundPlayer.PlaySync();
            else if (settings.PlayStartSound)
                startSoundPlayer.PlaySync();

            PlayBeat();
        }

        public void ChangeSpeed(int speed)
        {
            bpm = speed;
        }

        public void Stop()
        {
            doneTicks = 0;
            remainingTicks = 0;
            beatTimer.Change(Timeout.Infinite, Timeout.Infinite);
            playStopped.Set();

            OnStopped();

            if (settings.PlayEndSound)
            {
                int count = settings.EndSoundRepeats;

                if (settings.PlayVocals)
                    stopSoundPlayer.PlaySync();
                while (count-- > 0)
                    endSoundPlayer.PlaySync();
            }
        }

        private bool IsPlaying() => playStopped.WaitOne(0);
        private bool IsContinuousPlay() => durationTicks == ContinuousPlayMode;

        private void OnTick(object state)
        {
            PlayBeat();
        }

        private void PlayBeat()
        {
            SetTempoInterval();
            tickSoundPlayer.Play();
            doneTicks++;

            if (IsContinuousPlay())
            {
                OnMetronomeTicked(new MetronomeEventArgs(doneTicks, ContinuousPlayMode));
                return;
            }

            OnMetronomeTicked(new MetronomeEventArgs(doneTicks, remainingTicks));

            if (remainingTicks-- == 0)
            {
                Stop();
            }
        }

        private void SetTempoInterval()
        {
            var beatIntervalMilliseconds = (int) (1000.0 / (bpm / 60.0));
            beatTimer.Change(beatIntervalMilliseconds, beatIntervalMilliseconds);
        }
    }
}