﻿namespace FigrollApps.Pacer.Domain
{
    public class Settings
    {
        public int VerySlowSpeed { get; set; } = 30;
        public int SlowSpeed { get; set; } = 60;
        public int SteadySpeed { get; set; } = 90;
        public int NormalSpeed { get; set; } = 120;
        public int FastSpeed { get; set; } = 180;
        public int DoubleSpeed { get; set; } = 360;
        public string StartSound { get; set; } = "Korg-TR-Rack-African-Log-Drum-C4.wav";
        public string TickSound { get; set; } = "Roland-SC-88-Wood-Block.wav";
        public string EndSound { get; set; } = "A-Tone-His_Self-1266414414.wav";
        public bool PlayStartSound { get; set; } = true;
        public bool PlayEndSound { get; set; } = true;
        public bool PlayVocals { get; set; } = true;
        public int EndSoundRepeats { get; set; } = 1;
    }
}