﻿using System;
using System.ComponentModel;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using FigrollApps.Pacer.Annotations;

namespace FigrollApps.Pacer.Domain
{
    public sealed class PacerApp: INotifyPropertyChanged
    {
        private int speed;
        private Brush speedColor;
        private int count;

        private readonly Settings settings;
        private readonly Metronome metronome;
        
        public event EventHandler<EventArgs> PlayEnded;
        private void OnPlayEnded()
        {
            PlayEnded?.Invoke(this, EventArgs.Empty);
        }

        public PacerApp(Settings settings)
        {
            this.settings = settings;
            metronome = new Metronome(this.settings);

            metronome.Ticked += (sender, args) =>
            {
                Application.Current.Dispatcher.Invoke(() => { Count = args.TicksRemaining; });
            };

            metronome.Stopped += (sender, args) =>
            {
                Application.Current.Dispatcher.Invoke(OnPlayEnded);
            };
        }

        [UsedImplicitly]
        public int Speed
        {
            get => speed;
            private set
            {
                if (value == speed) return;
                speed = value;
                SetSpeedColor();
                OnPropertyChanged();
            }
        }

        private void SetSpeedColor()
        {
            if (Speed <= settings.SlowSpeed)
                SpeedColor = Brushes.DarkBlue;
            else if (Speed <= settings.NormalSpeed)
                SpeedColor = Brushes.DarkGreen;
            else
                SpeedColor = Brushes.DarkRed;
        }

        [UsedImplicitly]
        public int Count
        {
            get => count;
            private set
            {
                if (value == count) return;
                count = value;
                OnPropertyChanged();
            }
        }

        [UsedImplicitly]
        public Brush SpeedColor
        {
            get => speedColor;
            private set
            {
                if (value.Equals(speedColor)) return;
                speedColor = value;
                OnPropertyChanged();
            }
        }

        public void ChangeSpeed(int newSpeed)
        {
            Speed = newSpeed;
            metronome.ChangeSpeed(Speed);
        }

        public void ChangeCount(int delta)
        {
            Count += delta;
        }

        public void Start()
        {
            metronome.Play(Speed, Count);
        }

        public void Stop()
        {
            metronome.Stop();
        }

        public void ClearCount()
        {
            Count = 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
