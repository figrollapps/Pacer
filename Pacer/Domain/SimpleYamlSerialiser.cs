﻿using System;
using System.IO;
using System.Windows;
using YamlDotNet.Serialization;

namespace FigrollApps.Pacer.Domain
{
    public class SimpleYamlSerialiser<T> where T : class
    {
        public T Load(string filename)
        {
            var data = default(T);

            if (File.Exists(filename))
            {
                try
                {
                    using (var streamReader = new StreamReader(filename))
                    {
                        var deserializer = new Deserializer();
                        data = deserializer.Deserialize<T>(streamReader);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show($"Couldn't load settings file {filename}" + e.Message);
                }
            }

            return data;
        }

        public void Save(T data, string filename)
        {
            using (var streamWriter = new StreamWriter(filename))
            {
                var serializer = new Serializer();
                serializer.Serialize(streamWriter, data);
            }
        }
    }
}