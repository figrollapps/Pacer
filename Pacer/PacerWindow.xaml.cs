﻿using System.Reflection;
using System.Windows;
using System.Windows.Input;
using FigrollApps.Pacer.Domain;

namespace FigrollApps.Pacer
{
    /// <summary>
    /// Interaction logic for PacerWindow.xaml
    /// </summary>
    public partial class PacerWindow : Window
    {
        private readonly SimpleYamlSerialiser<Settings> loader = new SimpleYamlSerialiser<Settings>();
        private readonly PacerApp pacerApp;
        private readonly Settings settings;

        public PacerWindow()
        {
            InitializeComponent();

            settings = loader.Load("settings.yaml");

            pacerApp = new PacerApp(settings);
            pacerApp.PlayEnded += (sender, args) => { UpdateOnStop(); };

            this.DataContext = pacerApp;
            this.MouseDown += Window_MouseDown;

            NormalButton.IsChecked = true;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void VerySlow_OnChecked(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeSpeed(settings.VerySlowSpeed);
        }

        private void Slow_OnChecked(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeSpeed(settings.SlowSpeed);
        }

        private void Steady_OnChecked(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeSpeed(settings.SteadySpeed);
        }

        private void Normal_OnChecked(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeSpeed(settings.NormalSpeed);
        }

        private void Fast_OnChecked(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeSpeed(settings.FastSpeed);
        }

        private void Double_OnChecked(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeSpeed(settings.DoubleSpeed);
        }

        private void Clear_OnClick(object sender, RoutedEventArgs e)
        {
            pacerApp.ClearCount();
        }

        private void Add10_OnClick(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeCount(+10);
        }

        private void Add25_OnClick(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeCount(+25);
        }

        private void Add50_OnClick(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeCount(+50);
        }

        private void Minus_OnClick(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeCount(-1);
        }

        private void Plus_OnClick(object sender, RoutedEventArgs e)
        {
            pacerApp.ChangeCount(+1);
        }

        private void Start_OnClick(object sender, RoutedEventArgs e)
        {
            pacerApp.Start();

            StopButton.IsEnabled = true;
            StartButton.IsEnabled = false;

            ClearButton.IsEnabled = false;
            TenButton.IsEnabled = false;
            TwentyFiveButton.IsEnabled = false;
            FiftyButton.IsEnabled = false;
            PlusOneButton.IsEnabled = false;
            MinusOneButton.IsEnabled = false;
        }

        private void Stop_OnClick(object sender, RoutedEventArgs e)
        {
            pacerApp.Stop();
        }

        private void UpdateOnStop()
        {
            StopButton.IsEnabled = false;
            StartButton.IsEnabled = true;

            ClearButton.IsEnabled = true;
            TenButton.IsEnabled = true;
            TwentyFiveButton.IsEnabled = true;
            FiftyButton.IsEnabled = true;
            PlusOneButton.IsEnabled = true;
            MinusOneButton.IsEnabled = true;
        }

        private void Quit_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
